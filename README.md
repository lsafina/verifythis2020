# Casino with JavaBIP

JavaBIP contribution to the [Soldity Casino](https://verifythis.github.io/casino/) case study.

### Artifacts
- Java code
- JavaBIP executable specification

### Investigated properties
no properties yet / adherence to the protocol by coordinating interactions

### Verification Techniques
verification possible by model transformation into other input formalisms

### Looking for:
validation of abstraction hypotheses, identification of modelling bugs
 
### Authors
- Simon Bliudze (simon.bliudze@inria.fr), 
- Larisa Safina (larisa.safina@inria.fr)

### repository
https://gitlab.inria.fr/lsafina/verifythis2020/-/tree/main/src/main/java/casino
